"""Testing server."""

import json

from flask import Flask
from flask import request

app = Flask(__name__)

DEFAULT_USERS = {
        0: {
            'id': 0,
            'name': 'Bob',
            'emails': {
                0: {'id': 0, 'address': 'bob@mail.com'},
                3: {'id': 3, 'address': 'bob.s@protonmail.com'},
            },
            'phones': [
                {'id': 0, 'number': 5656456},
            ],
            'misc': {'uuid': '4141f2f2-727c-0d31-fd9f-b9d7469a7315'},
        },
        1: {
            'id': 1,
            'name': 'Alice',
            'emails': {
                1: {'id': 1, 'address': 'alice@mail.com'},
                2: {'id': 2, 'address': 'alice_other@mail.com'},
            },
            'phones': [
                {'id': 1, 'number': 12345},
            ],
            'misc': {'uuid': '1af0e646-4f9f-948d-b070-dd0217d8980f'},
        },
    }

USERS = DEFAULT_USERS.copy()


@app.route('/user/<int:user_id>', methods=['GET', 'PUT', 'DELETE'])
def get_user(user_id):
    """Test user enpoint."""
    if request.method == 'GET':
        return json.dumps(USERS[user_id])
    elif request.method == 'DELETE':
        USERS.pop(user_id)
        return '', 204
    elif request.method == 'PUT':
        USERS[user_id]['name'] = json.loads(request.data)['name']
        return USERS[user_id]


@app.route('/user/<int:user_id>/email')
def email_list(user_id):
    """Test the email list for a user_id."""
    return json.dumps(
        {'results': {'emails': list(USERS[user_id]['emails'].values())}}
    )


@app.route('/user/<int:user_id>/email/<int:email_id>')
def email(user_id, email_id):
    """Test an specific email id for a user_id."""
    return json.dumps(
        USERS[user_id]['emails'][email_id]
    )


@app.route('/user', methods=['GET', 'POST'])
def users():
    """Test the list of users."""
    if request.method == 'GET':
        return json.dumps(
            {'results': {'users': list(USERS.values())}}
        )

    elif request.method == 'POST':
        user = {
            'id': len(USERS),
            'name': None,
            'emails': [],
            'phones': [],
        }
        data = json.loads(request.data)
        user.update(data)

        USERS[len(USERS)] = user
        return json.dumps(user)


@app.route('/reset_default')
def reset_default():
    """Test reset to default."""
    USERS = DEFAULT_USERS.copy()  # noqa: F841
    return 200


if __name__ == '__main__':
    app.run(port=8888)
