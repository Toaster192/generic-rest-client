"""API Objects."""
from restclient import CREATEMethod
from restclient import DELETEMethod
from restclient import GETMethod
from restclient import LISTMethod
from restclient import ObjectDELETEMethod
from restclient import RESTManager
from restclient import RESTObject


class UserEmail(RESTObject):
    """UserEmail class."""

    _id = 'email_id'


class UserEmailManager(RESTManager, GETMethod, LISTMethod):
    """UserEmailManager class."""

    _obj_cls = UserEmail
    _path = 'user/{user_id}/email'
    _listed_by = 'emails'
    _from_parent_attrs = ('user_id:id', )


class UserPhone(RESTObject):
    """UserPhone class."""

    _id = 'phone_id'


class UserPhoneManager(RESTManager, GETMethod):
    """UserPhoneManager class."""

    _obj_cls = UserPhone
    _path = 'user/{user_id}/phone'
    _from_parent_attrs = ('user_id:misc.uuid', )


class User(RESTObject, ObjectDELETEMethod):
    """User class."""

    _managers = {
        'email': 'UserEmailManager',
        'phone': 'UserPhoneManager',
    }


class UserManager(RESTManager, GETMethod, LISTMethod, CREATEMethod, DELETEMethod):
    """UserManager class."""

    _obj_cls = User
    _path = 'user'
    _listed_by = 'users'
